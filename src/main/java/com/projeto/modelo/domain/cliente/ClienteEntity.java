package com.projeto.modelo.domain.cliente;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name = "CLI_cliente")
public class ClienteEntity implements Serializable {

	private static final long serialVersionUID = 6455633142199367050L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "CLI_nu_id")
	private Integer id;
	
	@ApiModelProperty(value = "Nome do cliente")
	@Column(name = "CLI_nome")
	private String nomeCliente;


	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getNomeCliente() {
		return nomeCliente;
	}


	public void setNomeCliente(String nomeCliente) {
		this.nomeCliente = nomeCliente;
	}


	
	
	
	


	
}
