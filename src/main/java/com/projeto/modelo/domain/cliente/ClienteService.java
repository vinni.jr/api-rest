package com.projeto.modelo.domain.cliente;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClienteService  {
	
	@Autowired
	private ClienteRepository repository;
	
	public ClienteEntity salvar(ClienteEntity cliente) {
		return repository.save(cliente);
	}

	public ClienteEntity obter(Integer id) {
		return repository.findById(id).orElse(null);
	}
}
