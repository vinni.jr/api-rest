package com.projeto.modelo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.projeto.modelo.domain.cliente.ClienteEntity;
import com.projeto.modelo.domain.cliente.ClienteService;

@RestController
@RequestMapping("/api/v1/cliente")
public class ClienteController {
	
	@Autowired
	private ClienteService service;
	
	@RequestMapping(value = "/salvar", method =  RequestMethod.POST, produces="application/json", consumes="application/json")
	public ResponseEntity<?> salvar(@RequestBody ClienteEntity cliente) {
		ClienteEntity clienteSalvo = new ClienteEntity();
		try{
			clienteSalvo = service.salvar(cliente);
		}catch (Exception e) {
			return ResponseEntity.status(HttpStatus.FORBIDDEN).body(e.toString());
		}
		return ResponseEntity.status(HttpStatus.CREATED).body(clienteSalvo);
	}
	
	@GetMapping(value = "/obter/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> buscarOne(@PathVariable("id") Integer id) {
		ClienteEntity clienteSalvo = new ClienteEntity();
		try{
			clienteSalvo = service.obter(id);
		}catch (Exception e) {
			return ResponseEntity.status(HttpStatus.FORBIDDEN).body(e.toString());
		}
		return ResponseEntity.status(HttpStatus.CREATED).body(clienteSalvo);
	}



}
